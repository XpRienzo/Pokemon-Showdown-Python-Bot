userdetails = {'nickname': '',
               'password': '',
               'avatar': ''}

rooms = [''] # Add rooms here

devs = ['xprienzo'] # Keep the usernames in id format, users here can access all commands.

server = 'sim2.psim.us' # Socket Address of the server you're trying to connect to. By default the bot expects it to support secure connections

comchar = ['+','.'] # List of command characters separated by commas.

ranks = " +%@★*#&~" # You can update the rank string if you need custom ranks to be supported.
